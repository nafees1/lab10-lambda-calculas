(define pred_pair
    (lambda (pair)
        ((lcons
            (lcdr pair))
            (succ (lcdr pair)))
    )
)
(define pred
    (lambda (n)
        (lcar
            ((n pred_pair)
               
                ((lcons zero) zero)
            )
        )
    )
)

(define subtract
    (lambda (n)
        (lambda (m)
            ((m pred) n)
        )
    )
)

(define is-zero
    (lambda (n)
            ((n (lambda (x) lfalse)) ltrue)
    )
)

(define leq
    (lambda (x)
            (lambda (y)
                    (is-zero ((subtract x) y))
            )
    )
)

;reference: wikipedia
; reference: shlomifish.org